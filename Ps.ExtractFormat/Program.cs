﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Ps.ExtractFormat
{
    enum VideoFormat
    { 
        wmv,
        avi,
        webm,
        mp4,
    }

    class Program
    {
        [DllImport("Kernel32.dll", CharSet = CharSet.Unicode)]
        static extern bool CreateHardLink(
            string lpFileName,
            string lpExistingFileName,
            IntPtr lpSecurityAttributes
        );

        static void Main(string[] args)
        {
            var path = @"h:\data\pluralsight\";

            var outPath = @"h:\data\pluralsight\_output";

            var videoGroups = GetVideoFileGroupsFrom(path).ToList();

            var count = videoGroups.Count;
            if (count == 0)
            {
                Console.WriteLine("No matching video files found");
                return;
            }

            var format = GetFormat(videoGroups);
            Console.WriteLine("You have chosen: {0}", format.Key);

            Extract(path, outPath, format);
        }

        private static IEnumerable<IGrouping<string, FileInfo>> GetVideoFileGroupsFrom(string path)
        {
            var includeDirs = File.ReadAllLines(@"d:\Users\TolyaN\Desktop\selection.txt").Select(l => l.TrimEnd('\\'));

            var files = new List<FileInfo>();

            foreach (var dir in includeDirs)
            {
                var en = new DirectoryInfo(Path.Combine(path, dir)).EnumerateFiles("*.*", SearchOption.AllDirectories);
                files.AddRange(en);
            }

            //files = new DirectoryInfo(path).EnumerateFiles("*.*", SearchOption.AllDirectories).ToList();

            files = files.Where(f => includeDirs.Any(d => f.FullName.Contains(d))).ToList();

            Console.WriteLine(string.Format("Files found: {0}", files.Count));

            var videoFiles = files.GroupBy(f => f.Extension.TrimStart('.'));

            videoFiles = videoFiles.Where(VideoFormatFilter);
            return videoFiles;
        }

        private static void Extract(string path, string outPath, IEnumerable<FileInfo> files)
        {
            var videoPaths = files.GroupBy(f => f.Directory.FullName.Replace(path, ""));
            foreach (var videoPath in videoPaths)
            {
                var folder = videoPath.Key.TrimEnd('/').TrimEnd('\\');
                var dirInfo = new DirectoryInfo(Path.Combine(path, videoPath.Key));
                
                if (Enum.GetNames(typeof(VideoFormat)).Any(v => v.Equals(dirInfo.Name)))
                {
                    folder = folder.Substring(0, folder.LastIndexOf('\\'));
                }
                
                var resultFolder = string.Format(Path.Combine(outPath, folder));
                Console.WriteLine(string.Format("Creating folder: {0}", resultFolder));
                Directory.CreateDirectory(resultFolder);
                foreach (var video in videoPath)
                {
                    PutVideo(resultFolder, video);
                }
            }
        }

        private static void PutVideo(string resultFolder, FileInfo video)
        {
            var resultRoot = Path.GetPathRoot(resultFolder);
            var originalRoot = Path.GetPathRoot(video.FullName);

            var destName = Path.Combine(resultFolder, video.Name);

            if (string.IsNullOrEmpty(resultRoot) == false &&
                char.IsLetter(resultRoot[0]) &&
                resultRoot == originalRoot)
            {
                CreateHardLink(destName, video.FullName, IntPtr.Zero);
            }
            else
            {
                File.Copy(video.FullName, destName);
            }
        }

        private static bool VideoFormatFilter(IGrouping<string, FileInfo> f)
        {
            return Enum.GetNames(typeof(VideoFormat)).Any(e => f.Key.Contains(e));
        }

        private static IGrouping<string, FileInfo> GetFormat(IEnumerable<IGrouping<string, FileInfo>> files)
        {
            IGrouping<string, FileInfo> fileInfo = null;
            do
            {
                Console.WriteLine("Pick the file format to extract:");
                int i = 1;
                foreach (var info in files)
                {
                    Console.WriteLine(string.Format("{0}. {1}", i++, info.Key));
                }
                Console.WriteLine("----------");
                Console.WriteLine("0. Exit");
                var input = Console.ReadLine();

                fileInfo = ProcessInput(files, fileInfo, input);
            }
            while (fileInfo == null);

            return fileInfo;
        }

        private static IGrouping<string, FileInfo> ProcessInput(
            IEnumerable<IGrouping<string, FileInfo>> files, 
            IGrouping<string, FileInfo> fileInfo, 
            string input)
        {
            int result;
            if (int.TryParse(input, out result))
            {
                if (result <= 0)
                {
                    Console.WriteLine("Terminating...");
                    Thread.Sleep(300);

                    Environment.Exit(0);
                }
                if (result <= files.Count())
                {
                    fileInfo = files.ElementAt(result - 1);
                }
                else
                {
                    Console.WriteLine("Wrong input - try again.");
                }
            }
            return fileInfo;
        }
    }
}
